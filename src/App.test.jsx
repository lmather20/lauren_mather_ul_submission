import React from "react";
import { render, fireEvent, screen, cleanup } from "@testing-library/react";
import MainPage from "./pages/mainPage";
import { db } from "./services/firebase";

describe("Some Simple Tests", () => {
  beforeEach((done) => {
    render(<MainPage />);
    done();
  });
  afterAll((done) => {
    db.close();
    done();
    cleanup();
  });

  test("The page should be correclty rendered", async (done) => {
    expect(screen.getByPlaceholderText("pageHeader")).toBeInTheDocument();
    done();
  });
  test("The Search bar is visible and can have input entered into it", async (done) => {
    expect(screen.getByPlaceholderText("search")).toBeInTheDocument();
    const searchInput = screen.getByPlaceholderText("search");
    fireEvent.change(searchInput, { target: { value: "Electrical" } });
    done();
  });
  test("The items can be filtered ascending and descending in price", async (done) => {
    expect(screen.getByPlaceholderText("select-sort")).toBeInTheDocument();
    const dropdown = screen.getByPlaceholderText("select-sort");
    fireEvent.click(dropdown);
    const dropdownOption = screen.getByPlaceholderText("descending");
    fireEvent.click(dropdownOption);
    done();
  });
});
