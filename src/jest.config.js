// jest.config.js
import { defaults } from "jest-config";
export const moduleFileExtensions = [
  ...defaults.moduleFileExtensions,
  "js",
  "jsx",
];
